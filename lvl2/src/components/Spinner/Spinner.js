import React from 'react';

import './Spinner.scss';

const Spinner = props => (
  <div className="lds-ring">
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default Spinner;
