import React from 'react';
import './ListFiles.scss';
import FileIcon from '../FileIcon/FileIcon';

const ListFiles = props => {
  const { files } = props;
  return (
    <div className="ListFiles">
      <ul className="list-group">
        {files.map((file, index) => {
          return (
            <li key={index} className="file-item list-group-item">
              <FileIcon />
              {file.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default ListFiles;
