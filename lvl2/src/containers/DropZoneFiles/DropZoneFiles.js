import React, { Component, Fragment } from 'react';

import './DropZoneFiles.scss';

import DragIcon from '../../components/DragIcon/DragIcon';
import ListFiles from '../../components/ListFiles/ListFiles';

import { connect } from 'react-redux';
import { uploadFileToServer } from '../../actions/uploadActions';
import ZoneError from '../../components/ZoneError/ZoneError';
import Spinner from '../../components/Spinner/Spinner';

class DropZoneFiles extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropOverd: false,
      files: []
    };
  }

  // On drop
  onDrap = event => {
    event.preventDefault();

    // set the layout UI
    this.setOver(false);

    if (event.dataTransfer.items) {
      let files = [];

      for (var i = 0; i < event.dataTransfer.items.length; i++) {
        if (event.dataTransfer.items[i].kind === 'file') {
          var file = event.dataTransfer.items[i].getAsFile();
          files.push(file);
        }
      }

      this.setState({ files }, this.onDropCompleteExtractFiles);
    } else {
      this.setState({ files: event.dataTransfer.files }, this.onDropCompleteExtractFiles);
    }
  };

  // On Drop over fundler
  onDragOver = event => {
    event.preventDefault();

    this.setOver(true);
  };

  // Set Over
  setOver = dropOverd => {
    this.setState({ dropOverd });
  };

  // drop start files
  onDropCompleteExtractFiles = () => {
    const { files } = this.state;

    // Extract files and Send It to Server
    let formdata = new FormData();

    files.map(file => formdata.append('file[]', file));

    this.props.uploadFileToServer(files);
  };

  // Render
  render() {
    const { dropOverd, files } = this.state;
    const { error, loading, success } = this.props;
    const dropEffects = dropOverd ? 'active' : '';

    let dropContent = null;
    let spinnerContent = null;
    let successContent = null;

    if (!files.length) {
      dropContent = (
        <div className="drop-description">
          <DragIcon />
          <p>Drop your document to Send</p>
        </div>
      );
    } else {
      dropContent = <ListFiles files={files} />;
    }

    if (loading) {
      spinnerContent = <Spinner />;
    }

    if (success) {
      const message = 'upload completed successfully.';

      successContent = (
        <div className="mt-2 alert alert-success" role="alert">
          {message}
        </div>
      );
    }

    return (
      <Fragment>
        <div
          className={`DropZoneFiles ${dropEffects}`}
          onDrop={this.onDrap}
          onDragOver={this.onDragOver}
        >
          {dropContent}
          {spinnerContent}
          {successContent}
        </div>

        <ZoneError error={error} />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ documents }) => ({
  error: documents.error,
  loading: documents.loading,
  success: documents.success
});

const mapDispatchToProps = dispatch => ({
  uploadFileToServer: filesData => dispatch(uploadFileToServer(filesData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DropZoneFiles);
