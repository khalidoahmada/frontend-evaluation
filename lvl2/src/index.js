import React from 'react';
import ReactDOM from 'react-dom';

//// Redux configs
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

// Instance of Axios
import axiosInstance from './axios';

// Reducers
import reducers from './reducers/index';

// CSS
import './index.css';
// App container
import App from './App';

// DEV ONLY config compose //////
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// DEV ONLY //////

// Application Store
const store = createStore(
  reducers,
  {},
  composeEnhancers(applyMiddleware(thunk.withExtraArgument(axiosInstance)))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
