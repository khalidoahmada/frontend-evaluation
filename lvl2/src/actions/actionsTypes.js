export const SEND_FILE_TO_SERVER = 'SEND_FILE_TO_SERVER';
export const API_FILES_UPLOAD_START = 'API_FILES_UPLOAD_START';
export const API_FILES_UPLOAD_END = 'API_FILES_UPLOAD_END';
export const API_FILES_UPLOAD_SUCCESS = 'API_FILES_UPLOAD_SUCCESS';
export const API_FILES_UPLOAD_ERROR = 'API_FILES_UPLOAD_ERROR';
