import * as actionsTypes from './actionsTypes';
import { API_FILES_UPLOAD } from '../config';

// Uploding Files action creator
const startUploading = () => ({
  type: actionsTypes.API_FILES_UPLOAD_START
});

// Upload Done
const endUploading = () => ({
  type: actionsTypes.API_FILES_UPLOAD_END
});

// Upload Error
const errorUpload = error => ({
  type: actionsTypes.API_FILES_UPLOAD_ERROR,
  error
});

// Success Upload
const successUpload = success => ({
  type: actionsTypes.API_FILES_UPLOAD_SUCCESS,
  success
});

/*
 * action Creator Upload files To Server
 **/
export const uploadFileToServer = files => {
  return (dispatch, getState, api) => {
    dispatch(startUploading());
    // empty success and error
    dispatch(errorUpload(null));
    dispatch(successUpload(null));

    api
      .post(API_FILES_UPLOAD, files)
      .then(response => {
        const { data } = response;

        let messageSuccess = 'Upload completed successfully.';

        if (data.id) {
          messageSuccess += ` Response id :${data.id}`;
        }
        dispatch(successUpload(messageSuccess));
      })
      .catch(error => {
        dispatch(errorUpload(error.message || 'Error on Sending Data'));
        console.log('Error Server >>> ', error);
      })
      .then(response => {
        dispatch(endUploading());
        return response;
      });
  };
};
