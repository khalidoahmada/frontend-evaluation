import OriginAxios from 'axios';
import { API_URL } from './config';

const axios = OriginAxios.create({
  baseURL: API_URL
});

export default axios;
