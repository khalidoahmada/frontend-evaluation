import React, { Component } from 'react';
import './App.css';
import DropZoneFiles from './containers/DropZoneFiles/DropZoneFiles';

class App extends Component {
  render() {
    return (
      <div className="App">
        <DropZoneFiles />
      </div>
    );
  }
}

export default App;
