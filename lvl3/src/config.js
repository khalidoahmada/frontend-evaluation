// API URL
export const API_URL = 'https://fhirtest.uhn.ca/baseDstu3';

// URL UPLOAD
export const API_FILES_UPLOAD = '/Binary';
