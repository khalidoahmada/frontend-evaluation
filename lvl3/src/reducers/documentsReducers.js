import * as actionsTypes from '../actions/actionsTypes';

const initialState = {
  success: null,
  loading: false,
  error: null
};

const documentsReducers = (state = initialState, action) => {
  switch (action.type) {
    case actionsTypes.API_FILES_UPLOAD_START:
      return {
        ...state,
        loading: true
      };

    case actionsTypes.API_FILES_UPLOAD_END:
      return {
        ...state,
        loading: false
      };
    case actionsTypes.API_FILES_UPLOAD_ERROR:
      return {
        ...state,
        error: action.error
      };
    case actionsTypes.API_FILES_UPLOAD_SUCCESS:
      return {
        ...state,
        success: action.success
      };
    default:
      return state;
  }
};

export default documentsReducers;
