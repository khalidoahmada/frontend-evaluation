import { combineReducers } from 'redux';
import documents from './documentsReducers';

export default combineReducers({
  documents
});
