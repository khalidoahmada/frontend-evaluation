import React, { Component } from 'react';

import './DropZoneFiles.scss';

import DragIcon from '../../components/DragIcon/DragIcon';
import ListFiles from '../../components/ListFiles/ListFiles';

class DropZoneFiles extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropOverd: false,
      files: []
    };
  }

  // On drop
  onDrap = event => {
    event.preventDefault();

    // set the layout UI
    this.setOver(false);

    if (event.dataTransfer.items) {
      let files = [];

      for (var i = 0; i < event.dataTransfer.items.length; i++) {
        if (event.dataTransfer.items[i].kind === 'file') {
          var file = event.dataTransfer.items[i].getAsFile();
          files.push(file);
        }
      }

      this.setState({ files });
    } else {
      this.setState({ files: event.dataTransfer.files });
    }
  };

  // On Drop over fundler
  onDragOver = event => {
    event.preventDefault();

    this.setOver(true);
    console.log('drag is Over', event);
  };

  // Set Over
  setOver = dropOverd => {
    this.setState({ dropOverd });
  };

  // Render
  render() {
    const { dropOverd, files } = this.state;

    const dropEffects = dropOverd ? 'active' : '';

    let dropContent = null;

    if (!files.length) {
      dropContent = (
        <div className="drop-description">
          <DragIcon />
          <p>Drop your document to Send</p>
        </div>
      );
    } else {
      dropContent = <ListFiles files={files} />;
    }

    return (
      <div
        className={`DropZoneFiles ${dropEffects}`}
        onDrop={this.onDrap}
        onDragOver={this.onDragOver}
      >
        {dropContent}
      </div>
    );
  }
}

export default DropZoneFiles;
