import { combineReducers } from 'redux';
import documentsReducers from './documentsReducers';

export default combineReducers({
    documentsReducers,
});
